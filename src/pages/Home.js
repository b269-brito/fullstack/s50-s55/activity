import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Button, Row, Col } from 'react-bootstrap';


export default function Home() {
	


	return (

		<>
		<Row>
    	<Col className="p-5">
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button variant="primary">Enroll now!</Button>
        </Col>
    </Row>
		
      	< Highlights/>
		</>
	)
}
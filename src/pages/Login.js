// import { Form, Button} from 'react-bootstrap';
// import {useState} from 'react';
// import {useEffect} from 'react';


// export default function Login() {
//     const [email, setEmail] = useState("");
//     const [password1, setPassword1] = useState("");
//     const [isActive, setIsActive] = useState(false);

//     useEffect(()=>{
//         if (email !== "" && password1 !== ""){
//             setIsActive(true);
//         } else {
//             setIsActive(false)
//         }
//     },[email, password1])

//     function loginUser(e){
//         (e.preventDefault());

//         //Clear input fields
//         setEmail("");
//         setPassword1("");

//         alert("Successful Login!");
//     };



//     return (
//         <Form onSubmit={(e)=> loginUser(e)} >
//             <Form.Group controlId="userEmail">
//             <h1> Login </h1>
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control 
// 	                type="email" 
// 	                placeholder="Enter email" 
//                     value={email}
//                     onChange={e=> setEmail(e.target.value)}
// 	                required
//                 />
//                 <Form.Text className="text-muted">
//                     We'll never share your email with anyone else.
//                 </Form.Text>
//             </Form.Group>

//             <Form.Group controlId="password1">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
// 	               type="password" 
//                     placeholder="Password"
//                     value= {password1}
//                     onChange={e => setPassword1(e.target.value)}  
//                     required
//                 />
//             </Form.Group>
//             <p></p>

//           {isActive?  <Button variant="success" type="submit" id="submitBtn" >
//                 Login
//             </Button> :  <Button variant="success" type="submit" id="submitBtn" disabled >
//                 Login
//             </Button>

//         }
//         </Form>
//     )

// }






import {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'

export default function Login(){
    // to store values of the input fields
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    const navigate = useNavigate();

    function authenticate(e){
        e.preventDefault()

        // Set the email of the authenticated user in the local storage
        // localStorage.setItem('propertyName', value)
        localStorage.setItem('email',email);

        setEmail('')
        setPassword('')
        navigate('/');

        alert("Successfully login!")
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
}

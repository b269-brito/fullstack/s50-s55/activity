// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
import {Link, NavLink} from 'react-router-dom'
import Nav from 'react-bootstrap/Nav';
import { Button, Row, Col } from 'react-bootstrap';
import React from 'react';
export default function Banner() {
// const banner = true;
// if (banner===false){
    return(
        <Row>
        <Col className="p-5">
            <h1>Error 404 - Page not found. </h1>
            <p>The page you are looking for cannot be found.</p>
            <Button variant="primary" >
            <Nav.Link as={NavLink} to="/">Back to Home</Nav.Link>
            </Button>
        </Col>
    </Row>);
    } 
//     else { 
//     return(    
    // <Row>
    // 	<Col className="p-5">
    //         <h1>Zuitt Coding Bootcamp</h1>
    //         <p>Opportunities for everyone, everywhere.</p>
    //         <Button variant="primary">Enroll now!</Button>
    //     </Col>
    // </Row>
// 	)
// }
// }